import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { FormModalComponent } from './form-modal/form-modal.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbButtonsModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import {ButtonModule} from 'primeng/button';
import {ChartModule} from 'primeng/chart';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {SidebarComponent} from './component/sidebar/sidebar.component';
import {AccountComponent} from './component/account/account.component';
import {AccountDetailSponsorComponent} from './component/account-detail-sponsor/account-detail-sponsor.component';
import {AccountDetailUserComponent} from './component/account-detail-user/account-detail-user.component';
import {AccountDetailTraderComponent} from './component/account-detail-trader/account-detail-trader.component';
import {EmailListComponent} from './component/email-list/email-list.component';
import {GratsComponent} from './component/grats/grats.component';
import {GratsDetailComponent} from './component/grats-detail/grats-detail.component';
import {HomeComponent} from './component/home/home.component';
import {OrderComponent} from './component/order/order.component';
import {OrderDetailComponent} from './component/order-detail/order-detail.component';
import {MessagerieComponent} from './component/messagerie/messagerie.component';
import {MessagerieConversationComponent} from './component/messagerie-conversation/messagerie-conversation.component';
import {ShopTypeComponent} from './component/shop-type/shop-type.component';
import {UploadComponent} from './component/upload/upload.component';
import {CheckboxModule} from 'primeng/checkbox';
import {AccountCreateUpdateSponsorComponent} from './component/account-create-update-sponsor/account-create-update-sponsor.component';
import { AccountUpdateSponsorComponent } from './component/account-update.sponsor/account-update.sponsor.component';
import {AccordionModule} from 'primeng/accordion';
import { AdminComponent } from './component/admin/admin.component';
import {NgFlashMessagesModule} from 'ng-flash-messages';
import { Page403Component } from './component/errorPage/page403/page403.component';
import { Page404Component } from './component/errorPage/page404/page404.component';
import { NgxLoadingModule } from 'ngx-loading';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {CardModule} from 'primeng/card';
import { AccountUnitComponent } from './component/account-unit/account-unit.component';
import { ListeMsgComponent } from './component/messagerie-conversation/liste-msg/liste-msg.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    AccountComponent,
    AccountDetailSponsorComponent,
    AccountDetailUserComponent,
    AccountDetailTraderComponent,
    EmailListComponent,
    GratsComponent,
    GratsDetailComponent,
    HomeComponent,
    OrderComponent,
    OrderDetailComponent,
    MessagerieComponent,
    MessagerieConversationComponent,
    ShopTypeComponent,
    UploadComponent,
    FormModalComponent,
    AccountCreateUpdateSponsorComponent,
    AccountUpdateSponsorComponent,
    AdminComponent,
    Page403Component,
    Page404Component,
    AccountUnitComponent,
    ListeMsgComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbCollapseModule,
    NgbButtonsModule,
    BrowserAnimationsModule,
    InputTextModule,
    PasswordModule,
    ButtonModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    CheckboxModule,
    AccordionModule,
    NgFlashMessagesModule,
    NgxLoadingModule,
    ChartModule,
    ToastModule,
    ProgressSpinnerModule,
    CardModule
  ],
  providers: [HttpClient, MessageService],
  exports: [
    SidebarComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

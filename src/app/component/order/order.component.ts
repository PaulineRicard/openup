import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../service/order/order.service';
import {Router} from '@angular/router';
import {DataOrderService} from '../../service/dataOrder/data-order.service';
import {NgFlashMessageService} from 'ng-flash-messages';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orders =  [];
  infoBoolean = false ;
  info: string;
  all = true;
  filterCollaps = true;
  isCollapsed2 = true;
  inputId: string;
  public loading = false;
  private status = [
    {
      name: 'waiting',
      select: false
    },
    {
      name: 'accepted',
      select: false
    },
    {
      name: 'pickup',
      select: false
    },
    {
      name: 'closed',
      select: false
    },
    {
      name: 'canceled',
      select: false
    },
    {
      name: 'refund',
      select: false
    }
  ];
  private types = [
    {
      name: 'user',
      content: 'livré ou livreur',
      select: false
    },
    {
      name: 'trader',
      content: 'commerçant',
      select: false
    },
    {
      name: 'order',
      content: 'commande',
      select: false
    }
  ];
  private filterStat = [];
  private filterType = '';
  constructor(private orderService: OrderService, private router: Router, private data: DataOrderService,
              private flash: NgFlashMessageService) { }

  ngOnInit() {
    // appel le service pour récuperer la liste de toutes les commandes
    this.orderService.getAllOrders().then(response => {
      // console.log(response);
      // stock la liste dans le service data
      this.data.stockOrders(Object.values(response));
      // défini le tableau pour le tri des commandes (si une commande possède n items elle sera récupérée n fois, donc
      // pour l'affichage, on séléctionne uniquement les commandes distinctes
      // @ts-ignore
      this.orders = this.orderService.getDistinctOrders(response);
      // on stock le tableau des commandes distincts dans le service data
      this.data.stockOrdersDistinct(this.orders);
      // console.log(this.orders);
    }).catch(reason => {
      // console.log(reason);
      // TODO prevoir reponses erreur de l'api
      if (reason.status === 403) {
        this.router.navigate(['403']);
      } else {
        this.flash.showFlashMessage({
          messages: ['Erreur interne !!'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'info'
        });
      }
    });
  }
// affiche toutes les commandes
  selectAll() {
    // console.log('all');
    this.all = true;
    this.unCheckedAll();
    this.orders = this.data.getOrdersDistinct();
    // console.log(this.orders);
  }
  // met a jour le tableau de filtre par status de commande
  filterStatus(i: number) {
    // console.log('filterStatus');
    if (this.status[i].select === true) {
      this.filterStat.splice(this.filterStat.indexOf(this.status[i].name), 1 );
    } else {
      this.filterStat.push(this.status[i].name);
    }
    this.status[i].select = !this.status[i].select;
    // console.log(this.filterStat);
  }
  // applique les filtres de status a la liste des commandes
  getFilterOrder() {
    // console.log(this.filterStat);
    // reduit la div recherche de filtres
    this.filterCollaps = true;
    // affiche le bouton "all"
    this.all = false;
    // met a jour la liste des mails a afficher
    const newList = this.orderService.getOrdersByStatus(this.orders, this.filterStat);
    this.orders = newList;
    // console.log('COUNT: // ' + newList.length);
  }
// remet a zero tous les filtres
  unCheckedAll() {
    // console.log('decocher tout');
    for (const stat of this.status) {
      stat.select = false;
    }
    for (const type of this.types) {
      type.select = false;
    }
    this.filterType = '';
    this.filterStat = [];
    this.inputId = '';
  }
  // recherche les commandes par user / trader / numero de commande
  getSearchOrder() {
    // console.log(this.inputId);
    // console.log(this.filterType);
// reduit la div recherche de filtres
    this.isCollapsed2 = true;
    // affiche le bouton "all"
    this.all = false;
    // met a jour la liste des mails a afficher
    const newList = this.orderService.getOrdersById(this.orders, this.filterType, this.inputId);
    if (newList.length > 0) {
      this.orders = newList;
    } else {
      this.info = 'Aucunne commande ne correspond a votre recherche';
      this.infoBoolean = true;
      this.orders = [];
    }
    this.orders = newList;
    // console.log('COUNT: // ' + newList.length);
  }
  // met a jour le tableau de filtre pour la recherche par id
  filterSearch(i: number) {
    // console.log('filtres de recherche par id!!!');
    // remet a zero les checkbox + le filtre
    for (const type of this.types) {
      type.select = false;
    }
    this.filterType = '';
    if (this.types[i].select === true) {
      this.filterType = '';
    } else {
      this.filterType = (this.types[i].name);
    }
    this.types[i].select = !this.types[i].select;
  }

  getOrderDetail(order: any) {
    this.loading = true;
    this.data.stockOrder(this.orderService.getAllOrdersById(order.id));
    // console.log(this.orderService.getAllOrdersById(order.id));
    // console.log('ORDER DELIVERY');
    // console.log(order.deliveryId);
    this.orderService.getAccountUser(order.deliveryId).then(response => {
      this.data.setDelivery(response);
      this.orderService.getAccountTrader(order.traderId).then(reponse => {
       this.data.setTrader(reponse);
       if (order.deliveryManId !== null) {
         this.orderService.getAccountUser(order.deliveryManId).then(rep => {
           this.data.setDeliveryMan(rep);
           this.loading = false;
           this.router.navigate(['admin/order', order.id]).then(r => console.log(r));
         }).catch(reason => {
           // tslint:disable-next-line:triple-equals
           if (reason.status == 403) {
             this.loading = false;
             this.router.navigate(['403']).then(r => console.log(r));
           } else {
             // TODO message flash erreur: ERREUR INTERNE - MERCI DE RECOMMENCER
             this.flash.showFlashMessage({
               messages: ['Erreur interne !!'],
               dismissible: true,
               timeout: 3000,
               // Type of flash message, it defaults to info and success, warning, danger types can also be used
               type: 'info'
             });
           }
         });
       } else {
         this.loading = false;
         this.router.navigate(['admin/order', order.id]).then(r => console.log(r));
       }
     }).catch(reason => {
        // tslint:disable-next-line:triple-equals
        if (reason.status == 403) {
          this.loading = false;
          this.router.navigate(['403']).then(r => console.log(r));
        } else {
          // TODO message flash erreur: ERREUR INTERNE - MERCI DE RECOMMENCER
          this.flash.showFlashMessage({
            messages: ['Erreur interne !!'],
            dismissible: true,
            timeout: 3000,
            // Type of flash message, it defaults to info and success, warning, danger types can also be used
            type: 'info'
          });
        }
      });
    }).catch(reason => {
      // tslint:disable-next-line:triple-equals
      if (reason.status == 403) {
        this.loading = false;
        this.router.navigate(['403']).then(r => console.log(r));
      } else {
        // TODO message flash erreur: ERREUR INTERNE - MERCI DE RECOMMENCER
        this.flash.showFlashMessage({
          messages: ['Erreur interne !!'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'info'
        });
      }
    });
  }
// permet de fermer les onglets de recherche: au clic sur un l'autre se ferme si il était ouvert
  collapse(coll) {
    if (coll === 'filterCollaps') {
      this.filterCollaps = !this.filterCollaps;
      this.isCollapsed2 = true;
    } else {
      this.isCollapsed2 = !this.isCollapsed2;
      this.filterCollaps = true;
    }
  }

  refresh() {
    this.ngOnInit();
  }
}

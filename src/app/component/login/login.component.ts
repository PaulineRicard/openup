import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {SecurityService} from '../../service/security/security.service';
import {Router} from '@angular/router';
import {NgFlashMessageService} from 'ng-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // FormGroup -> login form
  loginForm;
  constructor(private formBuilder: FormBuilder, private security: SecurityService, private router: Router,
              private flashMsg: NgFlashMessageService) {
    // construct form
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    });
  }

  ngOnInit() {
    // si l'utilisateur possède déjà un token et logged = true
    // il est redirigé vers home
    if (localStorage.getItem('logged') === 'true') {
      this.router.navigate(['admin/home']);
    }
  }

  login(data) {
    // console.log(data);
    // appel le service security pour connecter l'admin
    this.security.login(data).then(response => {
      // si tout se passe bien (status 200)
      //   console.log(response);
        // si le token se trouve dans le réponse
        // @ts-ignore
        if (response.token) {
          // créer une variable logged (verification du statut connecté pour le chargement des pages)
          localStorage.setItem('logged', 'true');
          localStorage.setItem('email', data.email);
          // sauvegarde le token dans le localStorage via le service security
          // @ts-ignore
          this.security.saveToken(response.token);
          // redirige vers la page Home
          this.router.navigate(['admin/home']).then(res => console.log('home' + res));
        } else {
          // si le token n'est pas présent dans le réponse, mais pas d'erreur
          // affichage d'un message flash pour informer l'utilisateur
          this.flashMsg.showFlashMessage({
            messages: ['Erreur Serveur, merci de réessayer...'],
            dismissible: true,
            timeout: 3000,
            // Type of flash message, it defaults to info and success, warning, danger types can also be used
            type: 'warning'
          });
        }
      // si la réponse de l'api n'est pas un status 200
    }).catch(reason => {
      // console.log(reason.status);
      if (reason.status === 401) {
        // utilisateur inconnu
        this.flashMsg.showFlashMessage({
          messages: ['Identifiants de connexion incorrects'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
      } else {
        // pour toute autre erreur venant de l'api
        this.flashMsg.showFlashMessage({
          messages: ['une erreur s\'est produite, merci de réessayer'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
      }
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GratsDetailComponent } from './grats-detail.component';

describe('GratsDetailComponent', () => {
  let component: GratsDetailComponent;
  let fixture: ComponentFixture<GratsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GratsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GratsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

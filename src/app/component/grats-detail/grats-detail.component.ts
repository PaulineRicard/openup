import { Component, OnInit, Input } from '@angular/core';
import { Grats } from 'src/app/model/Grats';
import { GratsService } from '../../service/grats/grats.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-grats-detail',
  templateUrl: './grats-detail.component.html',
  styleUrls: ['./grats-detail.component.css']
})
export class GratsDetailComponent implements OnInit {

  @Input() grat:Grats;

  constructor(private gratsService:GratsService, private modalService:NgbModal ) { }

  ngOnInit() {
  }

  openModal(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }
}

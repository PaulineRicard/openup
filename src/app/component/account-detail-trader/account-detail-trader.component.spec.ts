import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDetailTraderComponent } from './account-detail-trader.component';

describe('AccountDetailTraderComponent', () => {
  let component: AccountDetailTraderComponent;
  let fixture: ComponentFixture<AccountDetailTraderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDetailTraderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailTraderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

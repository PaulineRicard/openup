import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDetailSponsorComponent } from './account-detail-sponsor.component';

describe('AccountDetailSponsorComponent', () => {
  let component: AccountDetailSponsorComponent;
  let fixture: ComponentFixture<AccountDetailSponsorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDetailSponsorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

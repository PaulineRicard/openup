import {Component, Input, OnChanges} from '@angular/core';
import {Account} from '../../model/Account';
import {AccountService} from '../../service/account/account.service';
import {GratsService} from '../../service/grats/grats.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, Validators} from '@angular/forms';


@Component({
  selector: 'app-account-detail-sponsor',
  templateUrl: './account-detail-sponsor.component.html',
  styleUrls: ['./account-detail-sponsor.component.css']
})


export class AccountDetailSponsorComponent implements OnChanges {
@Input() roleClicked: string;
@Input() account: Account;

  private modalTitle: string;
  private sponsorForm;
  // true -> update /// false -> create
  private option: boolean;
  private sponsorAddressForm;

  constructor(private route: ActivatedRoute, private modalService: NgbModal, private accountService: AccountService,
              private gratsService: GratsService, private router: Router, private formBuilder: FormBuilder ) { }


  ngOnChanges() {
    // création du formulaire de la modale !!
    this.sponsorForm = this.formBuilder.group({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      role: new FormControl('sponsor'),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ])),
      phone: new FormControl('', Validators.minLength(10)),
      name: new FormControl('', Validators.required)
      // todo voir champs pour l'adresse !!!
    });
    this.sponsorAddressForm = this.formBuilder.group({
      address: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      zipcode: new FormControl('', Validators.compose(
        [Validators.required, Validators.minLength(5)]))
    });
  // console.log('component Sponsor')
  // console.log(this.roleClicked);
    // si le role correspond a la modale -> ouvre la modale en simulant le clic sur le bouton!!
    if (this.roleClicked === 'sponsor') {
      document.getElementById('btnShowSponsor').click();
      // si la propriété account n'esp pas undefined -> ouvre la modale avec les infos sur l'account!!!
      if (this.account !== undefined) {
        // todo masquer le bouton create
        // todo afficher le bouton update + bloquer le compte
        // todo préremplir les champs input avec les data de account
        // todo requête vers l'api pour récuperer l'addresse !!!
        this.modalTitle = `Compte Sponsor n°${this.account.id}`;
        this.option = true;
      } else {
        // todo ouverture de la modale pour créer un compte
        // todo afficher le bouton create
        // todo masquer le bouton update + bloquer le compte
        this.modalTitle = 'Création d\'un compte Sponsor';
        this.option = false;
      }
  }
  }

// au clic sur le bouton: affiche la modale
  openModal(content) {
    this.modalService.open(content).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });

  }


  // enmène vers la liste des gratifications
  goGrats() {

    this.router.navigate(['admin/grats']);

  }

  create(sponsor, address) {
    console.log('create');
  }
  update(sponsor, address) {
    sponsor.address = address;
    console.log(JSON.stringify(sponsor));
  }
  blocked(id) {
    console.log(id);
  }
}

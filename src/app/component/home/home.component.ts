import {Component, OnInit, ViewChild} from '@angular/core';
import {MessageService} from 'primeng/api';
import {OrderService} from '../../service/order/order.service';
import {AccountService} from '../../service/account/account.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // data for charts / graphic
  // chart order
data1;
// chart account
data2;
// graphique
data;
// loader (attente des charts data)
loader = false;
loader2 = false;
// au chargement du composant: récupère les données pour les charts
  ngOnInit(): void {
   this.getChartOrderdata();
   this.getChartAccountdata();
  }
  constructor(private messageService: MessageService, private orderService: OrderService, private accountService: AccountService) {
   // permet l'affichage des charts (primeng) ...
    this.data1 = {
      labels: ['waiting', 'accepted', 'pickup', 'closed', 'refund', 'canceled'],
      datasets: [
        {
          data: [1, 1, 1, 1, 1, 1],
          backgroundColor: [
            '#f36d2d',
            '#d88b75',
            '#aaa5b9',
            '#82b1db',
            '#e77d52',
            '#c59896',
          ],
          hoverBackgroundColor: [
            '#3b4151',
            '#3b4151',
            '#3b4151',
            '#3b4151',
            '#3b4151',
            '#3b4151'
          ]
        }]
    };
    this.data2 = {
    labels: ['User-waiting', 'User-validate', 'User-blocked', 'trader-waiting', 'trader-validate', 'trader-blocked'],
    datasets: [
      {
        data: [1, 1, 1, 1, 1, 1],
        backgroundColor: [
          '#f36d2d',
          '#d88b75',
          '#aaa5b9',
          '#21bdff',
          '#e77d52',
          '#c59896',
        ],
        hoverBackgroundColor: [
          '#3b4151',
          '#3b4151',
          '#3b4151',
          '#3b4151',
          '#3b4151',
          '#3b4151'
        ]
      }]
  };
    // todo set this.data.datasets[0] = tableau des commandes payées par journées de la semaine en cours
    // todo set this.data.datasets[1] = tableau des commandes remboursées par journées de la semaine en cours
    // affichage du graphique
    this.data = {
      labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
      datasets: [
        // donnés sur les paiementt reçus
        {
          label: 'paiements reçus',
          data: [10, 11, 25, 33, 9, 12, 13],
          fill: false,
          borderColor: 'darkorange'
        },
        // données sur les remboursements
        {
          label: 'remboursements',
          data: [0, 0, 1, 0, 2.2, 1, 0],
          fill: false,
          borderColor: '#565656'
        }
      ]
    };
}
  // affiche les données au passage de la souris sur le graphique
  selectData(event) {
    this.messageService.add({severity: 'info', summary: 'Data Selected',
      detail: this.data.datasets[event.element._datasetIndex].data[event.element._index]});
  }
// appel vers le service order pour récupérer les commandes, les trier et afficher les données dans le doghnut
  private getChartOrderdata() {
    // permettra de créer le tableau de données pour affichage
    let data = [];
    let refund = 0;
    let waiting = 0;
    let accepted = 0;
    let pickup = 0;
    let closed = 0;
    let canceled = 0;
    // récupère toutes les commandes via le service
    this.orderService.getAllOrders().then(response => {
      // tri les commandes pour ne pas avoir de doublons...
      // @ts-ignore
      const orders = this.orderService.getDistinctOrders(response);
      // pour chaque commande, en fonction de son status, on met a jour nos différentes variables
      for (const order of orders) {
        switch (order.status) {
          case 'waiting':
            waiting += 1;
            break;
          case 'accepted':
            accepted += 1;
            break;
          case 'refund':
            refund += 1;
            break;
          case 'pickup':
            pickup += 1;
            break;
          case 'closed':
            closed += 1;
            break;
          case 'canceled':
            canceled += 1;
            break;
          default: break;
        }
      }
      // met a jour le tableau avec les différentes variables
      data = [waiting, accepted, pickup, closed, refund, canceled];
      // console.log(data);
      // permet l'affichage des données récupérées
      this.data1.datasets[0].data = data;
      // masque le spinner
      setTimeout(() => { this.loader = !this.loader; } , 1700);
    });
  }
  // appel vers le service account pour récupérer les commandes, les triées et afficher les données dans le chart
  private getChartAccountdata() {
    // permettra de créer le tableau de données pour affichage
    let data = [];
    let userWaiting = 0;
    let userValidate = 0;
    let userBlocked = 0;
    let traderWaiting = 0;
    let traderValidate = 0;
    let traderBlocked = 0;
    this.accountService.getAllChart().then(rep => {
      const accounts = rep['hydra:member'];
      for (const account of accounts) {
        if (account.role === 'user') {
          switch (account.status) {
            case 'waiting':
              userWaiting += 1;
              break;
            case 'validate':
              userValidate += 1;
              break;
            case 'blocked':
              userBlocked += 1;
              break;
            default:
              break;
          }
        } else if (account.role === 'trader') {
          switch (account.status) {
            case 'waiting':
              traderWaiting += 1;
              break;
            case 'validate':
              traderValidate += 1;
              break;
            case 'blocked':
             traderBlocked += 1;
             break;
            default: break;
          }
        }
      }
      // met a jour le tableau avec les différentes variables
      data = [userWaiting, userValidate, userBlocked, traderWaiting, traderValidate, traderWaiting];
      // console.log(data);
      // permet l'affichage des données récupérées
      // masque le spinner
      setTimeout(() => {  this.data2.datasets[0].data = data; this.loader2 = !this.loader2; } , 1800);
    });
  }
}


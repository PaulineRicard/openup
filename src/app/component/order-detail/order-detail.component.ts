import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {OrderService} from '../../service/order/order.service';
import {DataOrderService} from '../../service/dataOrder/data-order.service';
import {NgFlashMessageService} from 'ng-flash-messages';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
  // id de la commande a afficher
  private id: number;
  // stock les infos sur la commande
  private order;
  // stock les infos sur le livreur
  private deliveryMan;
  // stock les infos sur le livré
  private delivery;
  // stock les infos sur le commerçant
  private trader;
  message: any;
  msg = false;
  // permet de masquer le btn refund
  refundBtn = true;
  // si order exist
  dataOrder = true;
  // si il y a un livreur
  private deliveryManExist = true;


  constructor(private route: ActivatedRoute, private orderService: OrderService, private data: DataOrderService,
              private router: Router, private flash: NgFlashMessageService) { }
// TODO voir récuperation des différentes infos !!
  ngOnInit() {
    // recupère l'id de la commande grace a l'url
     this.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
     // met a jour order / delivery / deliveryMan / trader grâce aux données stockées dans le service DataOrder
     this.order = this.data.getOrder();
     this.delivery = this.data.getDelivery();
     this.deliveryMan = this.data.getDeliveryMan();
   // si il n'y a pas de livreur (commande en attente) l'onglet livreur n'est pas dépliable
     if (this.deliveryMan === null) {
       this.deliveryManExist = false;
     }
     this.trader = this.data.getTrader();
     // si les données ne sont pas présentes au chargement: n'affiche rien sauf le bouton de retour a la liste des commandes
    // et un message d'erreur
     if (this.order === null) {
      this.dataOrder = false;
      this.refundBtn = true;
    } else {
       // si order.status != refund: le bouton rembourser est affiché
       // console.log(this.order[0].status)
       if (this.order[0].status !== 'refund') {
         this.refundBtn = false;
     }
    }
  }
// appel vers le service pour le remboursement d'une commande
  refund() {
    // TODO voir message flash!!
    this.orderService.refund(this.id).then( response => {
      this.msg = !this.msg;
      // console.log(response);
      this.order[0].status = 'refund';
      this.refundBtn = true;
      this.flash.showFlashMessage({
        messages: ['le remboursement a bien été éffectué !!'],
        dismissible: true,
        timeout: 3000,
        // Type of flash message, it defaults to info and success, warning, danger types can also be used
        type: 'success'
      });
    }).catch(reason => {
      // pris en compte des erreur de l'api
      // tslint:disable-next-line:triple-equals
      if (reason.status == 403) {
        this.router.navigate(['403']);
      } else {
        this.flash.showFlashMessage({
          messages: ['Erreur Serveur ... Merci de réessayer'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'alert'
        });
      }
    });
  }
// au clic sur le bouton -> redirige vers la liste des commandes
  goList() {
    this.router.navigate(['admin/order']);
  }
}

import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../../service/security/security.service';
import {Router} from '@angular/router';
import {MessagerieService} from '../../service/messagerie/messagerie.service';
import {AccountService} from '../../service/account/account.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  // affiche / masque la sidebar (en cours ....)
  private show = true;
  // affichage du mail de l'admin connecté
  private email: string;
  // affiche le nombre de nouveau(x) message(s)/account(s)
  newAccount: number;
  newMessage: number;
  // affiche les icones des notifications (messages / accounts)
  private notifMessage: boolean;
  private notifAccount: boolean;

  constructor(private security: SecurityService, private router: Router, private msgService: MessagerieService,
              private accountService: AccountService) { }

  ngOnInit() {
    this.email = localStorage.getItem('email');
    // récupère via les service le nombre de messages non lus et le nombre de comptes en attente de validation pour notif
    this.getNotifMessage();
    this.getNotifAccount();
  }

  // showSidebar() {
  //   this.show = !this.show;
  // }

  logout() {
    this.security.logout();
  }
  goAdmin() {
    this.router.navigate(['admin/admin']);
  }
  goEmail() {
    this.router.navigate(['admin/email']);
  }
  goHome() {
    this.router.navigate(['admin/home']);
  }
  goAccount() {
    this.router.navigate(['admin/account']);
  }
  goOrder() {
    this.router.navigate(['admin/order']);
  }
  goGrats() {
    this.router.navigate(['admin/grats']);
  }
  goMessagerie() {
    this.router.navigate(['admin/messagerie']);
  }
  goShopType() {
    this.router.navigate(['admin/shoptype']);
  }

  showSidebar() {
  this.show = !this.show;
  }
  getNotifMessage() {
    // todo requête vers le service messagerie pour récupérer les messages non lus
    this.msgService.getNews().then(response => {
      // console.log('SIDEBAR');
      // console.log(response);
      // todo this.newMessage : longueur de la réponse

      // @ts-ignore
      if (response.length > 0) {
        // @ts-ignore
        this.newMessage = response.length;
        this.notifMessage = true;
      } else {
        this.notifMessage = false;
      }
    });
  }
  getNotifAccount() {
    // todo requête vers le service account pour récupérer les comptes en attente de validation
    // todo this.newAccount : longueur de la réponse
    this.newAccount = 0;
    this.accountService.getAll().subscribe(response => {
      // console.log(response['hydra:member']);
      for (const account of response['hydra:member']) {
        if (account.status === 'waiting') {
          this.newAccount += 1;
        }
      }
      // console.log('NEWS: ' + this.newAccount);
      if (this.newAccount !== 0) {
        this.notifAccount = true;
      }
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountUnitComponent } from './account-unit.component';

describe('AccountUnitComponent', () => {
  let component: AccountUnitComponent;
  let fixture: ComponentFixture<AccountUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { AccountService } from '../../service/account/account.service';
import { Account } from '../../model/Account';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-account-unit',
  templateUrl: './account-unit.component.html',
  styleUrls: ['./account-unit.component.css']
})
export class AccountUnitComponent implements OnInit {

  @Input() account:Account

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { GratsService } from '../../service/grats/grats.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// import {ActivatedRoute} from '@angular/router';
import { Grats } from 'src/app/model/Grats';
import {Router} from '@angular/router';


@Component({
  selector: 'app-grats',
  templateUrl: './grats.component.html',
  styleUrls: ['./grats.component.css']
})

export class GratsComponent implements OnInit {

  grats: Grats[];
  @Input() grat:Grats;

  constructor(private gratsService:GratsService, private modalService:NgbModal,  private router:Router ) { }

  ngOnInit() {
    this.grats = this.gratsService.getAllGrats();
  }

  // goGrats() {
  //   this.router.navigate(['admin/grats']);
  // }
}

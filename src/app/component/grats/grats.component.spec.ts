import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GratsComponent } from './grats.component';

describe('GratsComponent', () => {
  let component: GratsComponent;
  let fixture: ComponentFixture<GratsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GratsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GratsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

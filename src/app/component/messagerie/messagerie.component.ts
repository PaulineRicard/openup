import { Component, OnInit } from '@angular/core';
import {MessagerieService} from '../../service/messagerie/messagerie.service';
import {NgFlashMessageService} from 'ng-flash-messages';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.css']
})
export class MessagerieComponent implements OnInit {
  all = true;
  isCollapsed = true;
  filter: string;
  // permet de desactiver le bouton "select all" si il n'y a aucun message
  private msg = true;
  types = [
    {
      name: 'lastname',
      content: 'Nom',
      select: false
    },
    {
      name: 'id',
      content: 'id utilisateur',
      select: false
    }
  ];
  inputSearch: string;
  private messages;
  // true si tous les messages sont sélectionnés, false par défaut
  selectAllMsg = false;
  private selectedMessage = [];
  private archived = false;
  private status: string;
  constructor(private msgService: MessagerieService, private flashMsg: NgFlashMessageService, private router: Router,
              private modalService: NgbModal) { }


  ngOnInit() {
    this.msgService.getAll().then(response => {
      // console.log(response);
      if (response === []) {
        this.messages = [];
        this.msg = false;
      } else {
        this.messages = response;
        // console.log(this.messages);
        // appel vers le service pour trier les msg et récupérer uniquement les msg non archivés
        this.msgService.sortMessages(this.messages);
        this.messages = this.msgService.getUnArchived();
        // console.log(this.messages);
        // ajout d'un prop 'conversationId' a chaque message
        for (const msg of this.messages) {
          const conversation = msg.conversation;
          const conversationId = (conversation.split('/'))[3];
          // console.log(conversationId);
          msg.conversationId = conversationId;
          // const userId = msg.
        }
      }
    }).catch(reason => {
      // console.log('ERROR');
      // console.log(reason.status);
      // tslint:disable-next-line:triple-equals
      if (reason.status == 403) {
        this.router.navigate(['403']).then(r => console.log(r));
        // tslint:disable-next-line:triple-equals
      } else if (reason.status == 404 || reason.status == 400) {
        this.flashMsg.showFlashMessage( {
          messages: ['Erreur interne, merci de réessayer'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
      } else {
        this.flashMsg.showFlashMessage({
          messages: ['Erreur inconnue'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
      }
    });
  }
  // sélectionne/déselectionne tous les messages
  selectAll() {
    // console.log('selectAll');
    // met a jour la variable selectAllMsg
    this.selectAllMsg = !this.selectAllMsg;
    // si tous les messages sont sélectionnés
    if (this.selectAllMsg === true) {
      for (const msg of this.messages) {
        // met a jour la propriété select de msg et push msg dans le tableau des messages selectionnés
        // @ts-ignore
        msg.select = true;
        this.selectedMessage.push(msg);
      }
      // met a jour la propriété select de msg et réinitialise le tableau des messages selectionnés
    } else {
      for (const msg of this.messages) {
        // @ts-ignore
        msg.select = false;
      }
      this.selectedMessage = [];
    }
    // console.log(this.messages);
  }
// liste les messages non lus
  getUnRead() {
    // console.log('unread');
    // affiche le bouton listAll
    this.all = false;
    // si la partie recherche par... est ouverte -> ferme cette partie
    this.isCollapsed = true;
    // appel le service pour avoir la liste des messages non lus
    this.messages = this.msgService.getUnRead(this.messages);
  }
  // affiche la liste de tous les messages
  ListeAll() {
    // permet de masquer le bouton list all
    this.all = true;
    // si la partie recherche par... est ouverte -> ferme cette partie
    this.isCollapsed = true;
    // récupère la liste des messages non archivés !!
    this.messages = this.msgService.getUnArchived();
    // unselect message
    this.selectedMessage = [];
    this.selectAllMsg = false;
    for (const msg of this.messages) {
      // @ts-ignore
      msg.select = false;
    }
    // affiche et masque les bouton 'archivage'
    this.archived = false;
  }
// archive les messages séléctionnés
  archive(stat) {
    // todo rechargement du composant this.ngOnInit()!!!!!!
    // si la partie recherche par... est ouverte -> ferme cette partie
    this.isCollapsed = true;
    this.all = false;
      // appel vers le service pour archiver/récuperer les messages (update du status)
    this.msgService.archived(this.selectedMessage, stat);
    // todo then => listAll();
  }
// au changement sur les checkbox modifie la propriété filter
  filterSearch(i: number) {
    // si la checkbox est déjà cochée, elle est déselectionnée
    if (this.types[i].select === true) {
      this.types[i].select = false;
      this.filter = '';
    } else {
      // remet toutes les checkbox a "zero"
      for (const type of this.types) {
        type.select = false;
      }
      // coche la case cliquée et défini le filtre
      this.types[i].select = true;
      this.filter = this.types[i].name;
    }
    // console.log(this.filter);
  }
// permet de chercher des messages par nom / id user
  getSearchMsg() {
    this.all = false;
    // console.log(this.inputSearch);
    // console.log(this.filter);
    this.messages = this.msgService.getByFilter(this.filter, this.inputSearch, this.messages);
    if (this.messages.length === 0) {
    this.flashMsg.showFlashMessage({
      messages: ['Auncun message ne correspond a votre recherche'],
      dismissible: true,
      timeout: 3000,
      // Type of flash message, it defaults to info and success, warning, danger types can also be used
      type: 'info'
    });
    // @ts-ignore
    setTimeout(this.ListeAll(), 3000);
  }
  }
// permet de sélectionner / déselectionner un message (checkbox)
  selectMsg(i: number) {
    // si le message est déjà séléctionné
    if (this.messages[i].select === true) {
      // retire le message du tableau des messages sélectionnés
      this.selectedMessage.splice(this.selectedMessage.indexOf(this.messages[i]), 1);
    } else {
      // sinon on ajoute le message au tableau
      this.selectedMessage.push(this.messages[i]);
    }
    // modifie la propriété select du message
    this.messages[i].select = !this.messages[i].select;
    // console.log(this.selectedMessage);
  }


  seeArchived() {
    this.all = false;
    this.archived = !this.archived;
    this.messages = this.msgService.getArchived();
  }

  refresh() {
    this.messages = [];
    this.msgService.refresh();
    this.ngOnInit();
    // this.msgService.create().then(response => {
    //   console.log(response);
    // }).catch(reason => {
    //   console.log(reason);
    // });
  }
}

import { Component, OnInit } from '@angular/core';
import {EmailService} from '../../service/email/email.service';
import {Router} from '@angular/router';
import {NgFlashMessageService} from 'ng-flash-messages';
import {SecurityService} from '../../service/security/security.service';
import {AccountService} from '../../service/account/account.service';


@Component({
  selector: 'app-email-list',
  templateUrl: './email-list.component.html',
  styleUrls: ['./email-list.component.css']
})

export class EmailListComponent implements OnInit {
  // liste de tous les mails
  private emails = [];
  // message d'info si besoin
  private info;
  // boolean pour afficher/masquer le bouton "all emails"
  private all = true;
  // boolean pour afficher / masquer la div info
  private infoBoolean = false;
  // boolean pour afficher / masquer la liste des filtres
  isCollapsed = true;
  // liste des status pour les mails
  private status = [
    {name: 'error',
    content: 'error',
    select: false},
    {name: 'send',
      content: 'send',
      select: false}
  ];
  // liste des types pour les mails
  private types = [
    {name: 'validation',
      content: 'Validation d\'un compte',
      select: false},
    {name: 'bill',
      content: 'Facturation',
      select: false},
    {name: 'reload',
      content: 'Relance',
      select: false},
    {name: 'refused',
      content: 'Compte refusé',
      select: false},
    {name: 'blocked',
      content: 'Compte bloqué',
      select: false},
    {name: 'grats',
      content: 'Envoi code gratifications',
      select: false},
  ];
  // tableau des status selectionnés
  private filterStat = [];
  // tableau des type selectionnés
  private filterType = [];
  // champs de la recherche par mail
  inputEmail: string;

  constructor(private mailService: EmailService, private router: Router, private flashMsg: NgFlashMessageService,
              private securityService: SecurityService, private accountService: AccountService) {
  }
// au chargement du composant, initialise le liste des emails pour l'affichage de la liste
  ngOnInit() {
    this.mailService.getAllEmail().then(response => {
      // si aucun email n'existe => status 204
      console.log(response);
      // this.emails = response;
      // @ts-ignore
      for (const email of response) {
        const arrayAccount = email.account.split('/');
        const account = arrayAccount[3];
        this.accountService.getById(account).then(reponse => {
         // @ts-ignore
          const mail = reponse.email;
          // Object.defineProperty(email, 'email', mail);
          email.email = mail;
          this.emails.push(email);
          console.log(this.emails);
        });
      }
      }).catch(reason => {
      // si l'api retourne une erreur 403 (accès non authorisé), redirection vers page 403!!
      if (reason.status === 403) {
        this.router.navigate(['403']);
      } else {
        // sinon problème de communication avec l'api: message d'info a l'utilisateur
        this.flashMsg.showFlashMessage({
          // Array of messages each will be displayed in new line
          messages: ['Erreur Interne !!'],
          // Whether the flash can be dismissed by the user defaults to false
          dismissible: true,
          // Time after which the flash disappears defaults to 2000ms
          timeout: 2000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'warning'
        });
      }
    });

  }
  // au clic sur bouton "all", affiche la liste de tous les mails
  // masque le bouton "all"
  // desélectionne tous les filtres
  selectAll() {
    this.all = true;
    this.infoBoolean = false;
    this.unCheckedAll();
    this.ngOnInit();
  }

  // rempli le tableau des filtres par status / supprime une entrée
  // appeler a la selection d'un filtre
  filterStatus(i: number) {
    if (this.status[i].select === true) {
    this.filterStat.splice(this.filterStat.indexOf(this.status[i].name), 1 );
    } else {
      this.filterStat.push(this.status[i].name);
    }
    this.status[i].select = !this.status[i].select;
  }
  // rempli le tableau des filtres par type / supprime une entrée
  // appeler a la selection d'un filtre
  filterTypes(j: number) {
    if (this.types[j].select === true) {
      this.filterType.splice(this.filterType.indexOf(this.types[j].name), 1);
    } else {
      this.filterType.push(this.types[j].name);
    }
    this.types[j].select = !this.types[j].select;
  }
  // unchecked all filter + réinitialise les tableaux de filtres
  unCheckedAll() {
    for (const stat of this.status) {
      stat.select = false;
    }
    for (const type of this.types) {
      type.select = false;
    }
    this.filterType = [];
    this.filterStat = [];
  }
// récupère la liste de tous les mails en fonction des filtres séléctionnés
  getFilterEmails() {
    // console.log('Status selected: ' + this.filterStat);
    // console.log('Types selected: ' + this.filterType);
    // reduit la div recherche de filtres
    this.isCollapsed = true;
    // affiche le bouton "all"
    this.all = false;
    // met a jour la liste des mails a afficher
    const newList = this.mailService.getEmailFilter(this.emails, this.filterStat, this.filterType);
    this.emails = newList;
    // console.log('COUNT: // ' + newList.length);
  }
// recherche de mails par email
  searchByEmail() {
    this.all = false;
    // console.log(this.inputEmail);
    const newList = this.mailService.getByEmail(this.inputEmail, this.emails);
    if (newList.length > 0) {
      this.emails = newList;
    } else {
      this.info = 'Aucun email ne correspond a votre recherche';
      this.infoBoolean = true;
      this.emails = [];
    }
    this.inputEmail = '';
  }
  refresh() {
    this.ngOnInit();
  }
}

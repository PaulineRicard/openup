import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MessagerieService} from '../../service/messagerie/messagerie.service';
import {AccountService} from '../../service/account/account.service';
import {NgFlashMessageService} from 'ng-flash-messages';
import {Router} from '@angular/router';


@Component({
  selector: 'app-messagerie-conversation',
  templateUrl: './messagerie-conversation.component.html',
  styleUrls: ['./messagerie-conversation.component.css']
})
export class MessagerieConversationComponent implements OnInit {
  // info sur l'user et l'account user
  private user;
  // stock les messages de la conversation
  private messages;

  constructor(private modalService: NgbModal, private service: MessagerieService,
              private accountService: AccountService, private flash: NgFlashMessageService,
              private router: Router) { }
  // id de la conversation
  @Input() id;
  closeResult: any;
  // info de la conversation
  private conversation;
  // ngModel -> input message a envoyé
  private msgToSend: string;
  ngOnInit() {
    console.log(this.id);
    // récupère les messages grace a l'id de la la conversation
    this.service.getMessagesByConversation(this.id).then(repMsg => {
      // console.log(repMsg);
      this.messages = repMsg;
    });
    // récupère la conversation grace a l'id
    this.service.getConversation(this.id).then(response => {
      // console.log(response);
      this.conversation = response;
      // récupère l'user grace a l'id
      this.service.getUser(this.conversation.userId).then(rep => {
        // console.log(rep);
        this.user = rep;
        // @ts-ignore
        // récupère l'id de l'account lié a l'user et appel du service pour récupérer les infos de l'account
        const arrayAccount = this.user.account.split('/');
        const account = arrayAccount[3];
        this.accountService.getById(account).then(reponse => {
          // console.log(reponse);
          // @ts-ignore
          this.user.account = reponse;
        }).catch(reason => console.log(reason));
      }).catch(reason => console.log(reason));
    }).catch(reason => console.log(reason));
  }
  open(content) {
    this.service.sortConversation(this.messages);
    this.messages = this.service.getUnArchivedConversation();
    console.log(this.messages);
    this.modalService.open(content, {size: 'lg', centered: true, scrollable: true}).result.then((result) => {
      // console.log(result);
    }, (reason) => {
      // console.log(reason);
    });
  }
  // appel le service pour archiver les messages de la conversastion (status = 'archived')
  archived() {
    // console.log('arch');
    console.log(this.conversation);
    console.log('CONVERSATION !!');
    // todo archiver la conversation entière et fermer la modale
  }
// appel le service pour créer le message
  send() {
    console.log(this.msgToSend);
    this.service.create(this.msgToSend, this.id).then(response => {
      // console.log(response);
      this.flash.showFlashMessage({
        messages: ['votre message a bien été envoyé'],
        dismissible: true,
        timeout: 2000,
        // Type of flash message, it defaults to info and success, warning, danger types can also be used
        type: 'success'
      });
      this.msgToSend = '';
      setTimeout(() => { this.ngOnInit(); }, 1000);
    }).catch(reason => {
      // console.log(reason);
      // tslint:disable-next-line:triple-equals
      if (reason.status == 403) {
        this.router.navigate(['403']).then(r => console.log(r));
      } else {
        // TODO message flash erreur: ERREUR INTERNE - MERCI DE RECOMMENCER
        this.flash.showFlashMessage({
          messages: ['Erreur interne !!'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'info'
        });
      }
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeMsgComponent } from './liste-msg.component';

describe('ListeMsgComponent', () => {
  let component: ListeMsgComponent;
  let fixture: ComponentFixture<ListeMsgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeMsgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

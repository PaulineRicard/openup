import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-liste-msg',
  templateUrl: './liste-msg.component.html',
  styleUrls: ['./liste-msg.component.css']
})
export class ListeMsgComponent implements OnInit {
@Input() conversation;
  constructor() { }

  ngOnInit() {
  }

}

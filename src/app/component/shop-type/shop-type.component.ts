import { Component, OnInit } from '@angular/core';
import {ShopService} from '../../service/shop/shop.service';
import {Router} from '@angular/router';
import {NgFlashMessageService} from 'ng-flash-messages';

@Component({
  selector: 'app-shop-type',
  templateUrl: './shop-type.component.html',
  styleUrls: ['./shop-type.component.css']
})
export class ShopTypeComponent implements OnInit {
  private name: string;
  private types;

  constructor(private serviceShop: ShopService, private router: Router, private flash: NgFlashMessageService) { }

  ngOnInit() {
    this.serviceShop.getAll().then(response => {
    //  console.log(response);
      this.types = response['hydra:member'];
      // console.log(this.types);
    }).catch(reason => {
      // console.log(reason);
    });
  }
// appel vers le service pour enregistrer un nouveau type de shop
  addShop() {
    // console.log(this.name);
    const name = {name: this.name};

    this.serviceShop.create(name).catch(reason => {
      // console.log(reason);
      if (reason.status === 403) {
        // console.log(403);
        this.router.navigate(['403']);
      } else if (reason.status === 200) {
        // console.log(200);
        // todo message flash create ok
        this.flash.showFlashMessage({
          messages: ['Type de commerce ajouter avec succès'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'success'
        });
      } else {
        // console.log(reason);
        this.flash.showFlashMessage({
          messages: ['Une erreur s\'est produite... Merci de réessayer'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'warning'
        });
      }
    });
  }

  refresh() {
    this.ngOnInit();
  }

  update(input, id) {
    const name = {
      name: input
    };
    this.serviceShop.update(name, id).then(response => {
      // console.log(response);
    }).catch(reason => {
      // console.log(reason);
      if (reason.status === 403) {
        // console.log(403);
        this.router.navigate(['403']);
      } else if (reason.status === 200) {
        // console.log(200);
        // todo message flash create ok
        this.flash.showFlashMessage({
          messages: ['Type de commerce mis à jour avec succès'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'success'
        });
      } else {
        // console.log(reason);
        this.flash.showFlashMessage({
          messages: ['Une erreur s\'est produite... Merci de réessayer'],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'warning'
        });
      }
    });
  }
}

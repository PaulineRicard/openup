import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { AccountService } from '../../service/account/account.service';
import { Account } from '../../model/Account';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})


export class AccountComponent implements OnInit {


  // @Input() accounts: Account[];
  // address_id:Address_id[];
  accounts: Account[];

   // liste de tous les accounts
   private names;
   // message d'info si besoin
   private info;
   // boolean pour afficher/masquer le bouton "all accounts"
   private all = true;
   // boolean pour afficher / masquer la div info
   private infoBoolean = false;
   // boolean pour afficher / masquer la liste des filtres
   isCollapsed = true;



   // liste des status
   private status = [
    {name: 'validate',
      content: 'Actif',
      select: false},
    {name: 'waiting',
      content: 'En attente',
      select: false},
    {name: 'blocked',
      content: 'bloqué',
      select: false},
  ];

  // liste des types pour les accounts
  private types = [
    {name: 'sponsor',
      content: 'Sponsors',
      select: false},
    {name: 'user',
      content: 'Utilisateurs',
      select: false},
    {name: 'trader',
      content: 'Traders',
      select: false},
  ];

   // tableau des type selectionnés
  private filterType = [];
  // champs de la recherche par name
  inputName: string;
  // prop mis a jour au clic sur le bouton detail !!
  roleClicked: string;
  accountClicked;
  constructor( private route: ActivatedRoute, private accountService: AccountService, private modalService: NgbModal,
               private router: Router ) { }
  // récupération des accounts via le getAll
  ngOnInit() {
   this.accountService.getAll().subscribe(accounts => {
     this.accounts = accounts['hydra:member'];
     console.log(this.accounts);
   });
  }

  addsponsor(account: Account) {
    this.accountService.addsponsor(account).subscribe(reponse => {
      this.accounts.push(reponse);
      console.log(reponse);
    });
  }

  // openDetail(account){

  //   if(account.role === "sponsor"){
  //     this.router.navigate(['admin/account/sponsor', account.id]);
  //     account.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
  //   }
  //   if(account.role === "user"){
  //     this.router.navigate(['admin/account/user', account.id]);
  //     account.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
  //   }
  //   if(account.role === "trader"){
  //     this.router.navigate(['admin/account/trader', account.id]);
  //     account.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
  //   }

  // }


  // au clic sur bouton "all", affiche la liste de tous les comptes
  // masque le bouton "all"
  // desélectionne tous les filtres
  selectAll() {
    this.all = true;
    this.infoBoolean = false;
    this.unCheckedAll();
    this.ngOnInit();
  }


  test() {
    alert('button clicked');
  }


  // rempli le tableau des filtres par type / supprime une entrée
  // appeler a la selection d'un filtre
  filterTypes(j: number) {
    if (this.types[j].select === true) {
      this.filterType.splice(this.filterType.indexOf(this.types[j].name), 1);
    } else {
      this.filterType.push(this.types[j].name);
    }
    this.types[j].select = !this.types[j].select;
  }


  // rempli le tableau des filtres par status / supprime une entrée
  // appeler a la selection d'un filtre
  filterStatus(i: number) {
    if (this.status[i].select === true) {
      this.filterType.splice(this.filterType.indexOf(this.status[i].name), 1);
    } else {
      this.filterType.push(this.status[i].name);
    }
    this.status[i].select = !this.status[i].select;
  }


  // unchecked all filter + réinitialise les tableaux de filtres
  unCheckedAll() {

    for (const type of this.types) {
      type.select = false;
    }
    this.filterType = [];
    // this.filterStat = [];
  }


  // récupère la liste de tous les accounts en fonction des filtres séléctionnés
  getFilterAccounts() {
    // console.log('Status selected: ' + this.filterStat);
    console.log('Types selected: ' + this.filterType);
    // reduit la div recherche de filtres
    this.isCollapsed = true;
    // affiche le bouton "all"
    this.all = false;
    // met a jour la liste des mails a afficher
    const newList = this.accountService.getAccountsByType(this.accounts, this.filterType);
    this.accounts = newList;
    // console.log('COUNT: // ' + newList.length);
  }


  // recherche des accounts par noms et emails (recherche a chaque changement)
  // searchByName() {
  //   this.all = false;
  //   console.log(this.inputName);
  //   const newList = this.accountService.getByNameEmail(this.inputName, this.accounts);
  //   if (newList.length > 0) {
  //     this.accounts = newList;
  //   } else {
  //     this.info = ' Nom ne correspond a votre recherche';
  //     this.infoBoolean = true;
  //     this.accounts = [];
  //   }
  //   this.inputName = '';
  // }

// recharge le component
  refresh() {
    this.ngOnInit();
  }
// affiche uniquement les comptes en attente de validation
  getWaitingAccounts() {
    const newList = [];
    // pour chaque account, on verifie le status pour récuperer uniquement les comptes en attente de validation
    for (const account of this.accounts) {
      if (account.status === 'waiting') {
        newList.push(account);
      }
    }
    this.accounts = newList;
    // affiche le bouton "all"
    this.all = false;
  }

  showDetail(account: Account) {
    this.roleClicked = account.role;
    this.accountClicked = account;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountCreateUpdateSponsorComponent } from './account-create-update-sponsor.component';

describe('AccountCreateUpdateSponsorComponent', () => {
  let component: AccountCreateUpdateSponsorComponent;
  let fixture: ComponentFixture<AccountCreateUpdateSponsorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountCreateUpdateSponsorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountCreateUpdateSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

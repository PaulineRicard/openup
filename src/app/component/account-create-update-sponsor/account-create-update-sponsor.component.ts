import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Account } from '../../model/Account';
// import { Address_id } from '../../model/Address_id';
import { AccountService } from '../../service/account/account.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// import { Address } from 'cluster';

@Component({
  selector: 'app-account-create-update-sponsor',
  templateUrl: './account-create-update-sponsor.component.html',
  styleUrls: ['./account-create-update-sponsor.component.css']
})
export class AccountCreateUpdateSponsorComponent implements OnInit {

  account:Account;

  // address_id:Address_id;

@Output() addsponsor: EventEmitter<any> = new EventEmitter();
  // @Input() account: Account;
  id:number;
  firstname:string;
  lastname:string;
  email:string;
  role:string;
  status:string;
  password:string;
  phone:number;
  address:string;
  city:string;
  zipcode:string;
  name:string;



  constructor(private modalService: NgbModal, private accountService:AccountService) { }

  ngOnInit() {
    // this.account = this.accountService.getAll();
  }

  openModal(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onSubmit(){

    const account = {
      id: this.id,
      firstname: this.firstname,
      lastname: this.lastname,
      email: this.email,
      role: this.role,
      status: this.status,
      password: this.password,
      phone: this.phone,
      address: {
        address: this.address,
        city: this.city,
        zipcode: this.zipcode
      },
      name: this.name
    }
    this.addsponsor.emit(account);
  }
}

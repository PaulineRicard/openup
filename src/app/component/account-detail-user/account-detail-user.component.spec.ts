import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDetailUserComponent } from './account-detail-user.component';

describe('AccountDetailUserComponent', () => {
  let component: AccountDetailUserComponent;
  let fixture: ComponentFixture<AccountDetailUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDetailUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

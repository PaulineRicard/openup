import { Component, OnInit, Input } from '@angular/core';
import { Account } from '../../model/Account';
import { AccountService } from '../../service/account/account.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-account-detail-user',
  templateUrl: './account-detail-user.component.html',
  styleUrls: ['./account-detail-user.component.css']
})
export class AccountDetailUserComponent implements OnInit {
  
  @Input() account: Account;
  // id:number;
  // firstname:string;
  // lastname:string;
  // email:string;
  // role:string;
  // status:string;
  // password:string;
  // phone:number;
  // address_id:number;

  constructor(private modalService: NgbModal, private accountService: AccountService) { }

  ngOnInit() {
    // this.firstname = this.account.firstname;

    // this.lastname = this.account.lastname;
    // // @ts-ignore
    // this.email = this.account.email;
    // // @ts-ignore
    // this.password = this.account.password;
    // // @ts-ignore
    // this.role = this.account.role;

    // this.status = this.account.status;
    // // @ts-ignore
    // this.phone = this.account.phone;

    // this.id = this.account.id;

    // this.address_id = this.account.address_id;
  }

  openModal(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { Account } from '../../model/Account';
import { AccountService } from '../../service/account/account.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-account-update-sponsor',
  templateUrl: './account-update.sponsor.component.html',
  styleUrls: ['./account-update.sponsor.component.css']
})
export class AccountUpdateSponsorComponent implements OnInit {

  @Input() account: Account;

  constructor(private modalService: NgbModal, private accountService:AccountService) { }

  ngOnInit() {
  }

  openModal(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

}

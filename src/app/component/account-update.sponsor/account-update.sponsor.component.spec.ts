import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountUpdateSponsorComponent } from './account-update.sponsor.component';

describe('AccountUpdateSponsorComponent', () => {
  let component: AccountUpdateSponsorComponent;
  let fixture: ComponentFixture<AccountUpdateSponsorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountUpdateSponsorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountUpdateSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

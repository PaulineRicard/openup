import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../../service/security/security.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import { NgFlashMessageService } from 'ng-flash-messages';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  // admins: contient tous les admins pour affichage de la liste
  admins: any;
  // FormGroup -> adminForm
  adminForm: any;
  // stock les messages pour la validation du formulaire de création d'admin
  private validationMessage = {
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    password: [
      { type: 'required', message: 'Password is required' },
      { type: 'minlength', message: 'Password must be at least 5 characters long' }
      ]
};

  constructor(private securityService: SecurityService, private formBuilder: FormBuilder,
              private flashMsg: NgFlashMessageService, private router: Router) {
    // construction du formulaire de création d'admin: FormControl + Validators
    this.adminForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ]))
    });
  }
  // au chargement du composant, appel vers le service security pour récupérer la liste des admins
  ngOnInit() {
    this.securityService.getAll().then(response => {
      // @ts-ignore
      console.log(response['hydra:member']);
        // @ts-ignore
      this.admins = response['hydra:member'];
    }).catch(reason => {
      // si l'api retourne une erreur 403 (accès non authorisé), redirection vers page 403!!
      if (reason.status === 403) {
        this.router.navigate(['403']);
      } else {
        // sinon problème de communication avec l'api: message d'info a l'utilisateur
        this.flashMsg.showFlashMessage({
          // Array of messages each will be displayed in new line
          messages: ['Erreur Interne !!'],
          // Whether the flash can be dismissed by the user defaults to false
          dismissible: true,
          // Time after which the flash disappears defaults to 2000ms
          timeout: 2000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'warning'
        });
      }
      // console.log(reason);
    });
  }
// appeler au clic sur le bouton -> appel le service security pour créer un admin
  addAdmin(formData) {
    this.securityService.create(formData).then(response => {
      // console.log(response);
      // si tout se passe bien: affichage d'un message flash pour informer l'utilisateur
      this.flashMsg.showFlashMessage({
          // Array of messages each will be displayed in new line
          messages: ['Administrateur créer avec succès'],
          // Whether the flash can be dismissed by the user defaults to false
          dismissible: true,
          // Time after which the flash disappears defaults to 2000ms
          timeout: 2000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'success'
        });
      // Et rechargement du composant (mise a jour de la liste affichée)
      this.ngOnInit();
      // sinon echec de la création
      }).catch(reason => {
        // si il y a des erreur, flashMessage pour avertir de l'erreur
      console.log(reason.status);
      if (reason.status === 400) {
        this.flashMsg.showFlashMessage({
          // Array of messages each will be displayed in new line
          messages: ['Erreur: L\'adresse mail est deja utilisée'],
          // Whether the flash can be dismissed by the user defaults to false
          dismissible: true,
          // Time after which the flash disappears defaults to 2000ms
          timeout: 2000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
      } else {
        this.flashMsg.showFlashMessage({
          // Array of messages each will be displayed in new line
          messages: ['Une erreur s\'est produite, merci de réessayer'],
          // Whether the flash can be dismissed by the user defaults to false
          dismissible: true,
          // Time after which the flash disappears defaults to 2000ms
          timeout: 2000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
      }
    });
  }
  // au clic sur le bouton refresh (actualise le composant)
  refresh() {
    this.ngOnInit();
  }
}

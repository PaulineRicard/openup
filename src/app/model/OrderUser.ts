export default class OrderUser {
  user: {
    '@context': string
    '@id': string
    '@type': string
    XCoordinate: number
    YCoordinate: number
    account: string
    balanceWallet: number
    gratification: [];
    id: 1;
    orders: [];
    wallets: []
  };
  account: {
    '@context': string
    '@id': string
    '@type': string
    address: []
    email: string
    firstname: string
    id: number
    lastname: string
    phone: number
    role: string
    status: string
  };
  address: {
    '@context': string
    '@id': string
    '@type': string
    address: string
    city: string
    id: number
    zipcode: number
  };
}

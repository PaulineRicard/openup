export default class Message {
'@id': string;
'@type': string;
  content: string;
  createdAt: string;
  id: number;
  statusAdmin: string;
  statusUser: string;
}

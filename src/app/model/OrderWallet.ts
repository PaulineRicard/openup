export default class OrderWallet {
  wallet: {
    '@context': string
    '@id': string
    '@type': string
    amount: number
    createdAt: string
    id: number
    orders: []
    user: string
  };
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Account } from '../../model/Account';

@Injectable({
  providedIn: 'root'
})
export class GratsService {

  apiUrl = 'http://localhost:8000';

  constructor( private http:HttpClient ) { }
  

  getAllGrats() {
    // return this.http.get(this.url, this.headers).toPromise();
    // return this.http.get('https://api.themoviedb.org/3/search/movie?api_key=1bded0cf5ec81699b719a0ab217e461e&query=batman').toPromise();
    return [

              {
                id:1,
                name:"Montre Rolex",
                sponsor_id: 1,
                sponsor_account_id: 3,
                sponsor_account_address_id: 3,
              },
              {
                id:2,
                name:"bague en Or",
                sponsor_id: 1,
                sponsor_account_id: 3,
                sponsor_account_address_id: 3,   
              },
              {
                id:3,
                name:"Diamant Rouge",
                sponsor_id: 2,
                sponsor_account_id: 5,
                sponsor_account_address_id: 5,              
              },
              {
                id:4,
                name:"Ruby",
                sponsor_id: 2,
                sponsor_account_id: 5,
                sponsor_account_address_id: 5,             
              },
              {
                id:5,
                name:"dent en argent",
                sponsor_id: 3,
                sponsor_account_id: 7,
                sponsor_account_address_id: 7,
              },
              {
                id:6,
                name:"chaine de bronze",
                sponsor_id: 3,
                sponsor_account_id: 7,
                sponsor_account_address_id: 7,
              },
              {
                id:7,
                name:"Montre Rolex",
                sponsor_id: 4,
                sponsor_account_id: 9,
                sponsor_account_address_id: 9,
              },
              {
                id:8,
                name:"bague en Or",
                sponsor_id: 4,
                sponsor_account_id: 9,
                sponsor_account_address_id: 9,   
              },
              {
                id:9,
                name:"Diamant Rouge",
                sponsor_id: 5,
                sponsor_account_id: 11,
                sponsor_account_address_id: 11,              
              },
              {
                id:10,
                name:"Ruby",
                sponsor_id: 5,
                sponsor_account_id: 11,
                sponsor_account_address_id: 11,             
              },
              {
                id:11,
                name:"dent en argent",
                sponsor_id: 6,
                sponsor_account_id: 12,
                sponsor_account_address_id: 12,
              },
              {
                id:12,
                name:"chaine de bronze",
                sponsor_id: 6,
                sponsor_account_id: 12,
                sponsor_account_address_id: 12,
              },
          
          
           ];

  }

}

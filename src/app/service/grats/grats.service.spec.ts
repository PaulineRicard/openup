import { TestBed } from '@angular/core/testing';

import { GratsService } from './grats.service';

describe('GratsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GratsService = TestBed.get(GratsService);
    expect(service).toBeTruthy();
  });
});

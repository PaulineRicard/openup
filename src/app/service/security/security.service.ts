import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  /*
   url: API url
   loginHeaders: define headers to send request to API at connexion
   header: define headers to send request to API (tkn)
  */
  url = 'http://127.0.0.1:8000/api';
  loginHeaders = {headers: new HttpHeaders({
      'Content-Type': 'application/json'})};
  header = {headers: new HttpHeaders({
      'Content-Type': 'application/json',
     // Authorization: `Bearer ${this.getToken()}`
    })};
  constructor(private http: HttpClient, private router: Router) { }

  // login(): send form data to API, receive status 200 + token / 401 (unknown user!!)
  login(data) {
    console.log(data);
    return this.http.post(`${this.url}/login_check`, data, this.loginHeaders).toPromise();
  }
  // logout(): remove token from localStorage, redirect to login page.
  logout() {
    localStorage.removeItem('tkn');
    localStorage.removeItem('logged');
    localStorage.removeItem('email');
    this.router.navigate(['']).then(r => console.log('logout'));
  }
  // getToken(): return token store in localStorage
  getToken() {
    return localStorage.getItem('tkn');
  }
  // saveToken(): save token in LocalStorage
  saveToken(tkn) {
    localStorage.setItem('tkn', tkn);
  }
  // create(): to create an admin account
  create(formData) {
    return this.http.post(`${this.url}/admins`, formData, this.header).toPromise();
  }
  // getAll(): get all admin accounts
  getAll() {
    return this.http.get(`${this.url}/admins`, this.header).toPromise();
  }
}

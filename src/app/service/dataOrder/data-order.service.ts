import { Injectable } from '@angular/core';
import OrderUser from '../../model/OrderUser';
import OrderWallet from '../../model/OrderWallet';

@Injectable({
  providedIn: 'root'
})
export class DataOrderService {

  private order = null;
  private delivery;
  private deliveryMan = null;
  private trader;
  private orders = null;
  private ordersDistinct = null;
  constructor() { }
  getOrder() {
    return this.order;
  }
  stockOrder(order) {
    this.order = order;
  }
  getOrders() {
    return this.orders;
  }
  stockOrders(orders) {
    this.orders = orders;
  }
  getOrdersDistinct() {
    return this.ordersDistinct;
  }
  stockOrdersDistinct(orders) {
    this.ordersDistinct = orders;
  }
  setDeliveryMan(accountUser) {
    this.deliveryMan = accountUser;
    console.log(this.deliveryMan);
  }
  getDeliveryMan() {
    return this.deliveryMan;
  }
  setDelivery(accountUser) {
    this.delivery = accountUser;
    console.log(this.delivery);
  }
  getDelivery() {
    return this.delivery;
  }
  setTrader(trader) {
    this.trader = trader;
  }
  getTrader() {
    return this.trader;
  }
}

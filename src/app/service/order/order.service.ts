import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SecurityService} from '../security/security.service';
import {DataOrderService} from '../dataOrder/data-order.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private token = this.security.getToken();
  // headers.append('Authorization', `Bearer ${tokenParse}`);
  private url = 'http://127.0.0.1:8000';
  private headers = {headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // Authorization: `Bearer ${this.token}`
    })};
  user;
  constructor(private http: HttpClient, private security: SecurityService, private data: DataOrderService) { }

  // appel vers l'api -> liste de toutes les commandes
  getAllOrders() {
    return this.http.get(`${this.url}/api/orders`, this.headers).toPromise();
  }
  getAllOrdersById(id) {
    const orders = this.data.getOrders();
    const array = [];
    console.log('ARRAY');
    console.log(array);
    for (const order of orders) {
      if (order.id === id) {
        // @ts-ignore
        array.push(order);
      }
    }
    console.log('ARRAY');
    console.log(array);
    return array;
  }
  // tri les orders et renvoi les orders ou le status est compris dans le tableau des filtres
  getOrdersByStatus(orders: any, filterStat: any[]) {
    let orderByStatus = [];
    // si il y au moins un filtre status checked
    if (filterStat.length > 0) {
      for (const order of orders) {
        // pour chaque order de la liste si le status de la commande correspond a un filtre push dans ordersByStatus
        if (filterStat.indexOf(order.status) !== -1) {
          orderByStatus.push(order);
        }
      }
    } else {
      orderByStatus = orders;
    }
    return orderByStatus;
  }
// TODO refaire recherche après recuperation  des infos ....
  getOrdersById(orders: any, filterType: string, input) {
    let newList = [];
    switch (filterType) {
      case 'user' :
        for (const order of orders) {
          // pour chaque order de la liste si le status de la commande correspond a un filtre push dans ordersByStatus
          if (order.deliveryId === input || order.deliveryManId === input) {
            newList.push(order);
          }
        }
        break;
      case 'trader':
        for (const order of orders) {
          // pour chaque order de la liste si le status de la commande correspond a un filtre push dans ordersByStatus
          if (order.traderId === input) {
            newList.push(order);
          }
        }
        break;
      case 'order':
        for (const order of orders) {
          // pour chaque order de la liste si le status de la commande correspond a un filtre push dans ordersByStatus
          if (order.id === input) {
            newList.push(order);
          }
        }
        break;
      default :
        newList = orders;
    }
    return newList;
  }
  // remboursement d'une commande (update le wallet et update status commande)
  refund(id: any) {
    return this.http.get(`${this.url}/api/order/refund/${id}`).toPromise();
  }
// TODO a modifier user_id !!!!!!!!!!
  getAccountUser(userId) {
    return this.http.get(`${this.url}/api/users/${userId}/account`).toPromise();
  }
  getAccountTrader(traderId) {
    return this.http.get(`${this.url}/api/traders/${traderId}/account`).toPromise();
  }

  getDistinctOrders(orders: []) {
    const ids = [-1];
    const newOrders = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < orders.length; i++) {
      // si l'id de la commande est dans le tableau ids on passe, sinon on ajoute l'id de la commande a ids et la commande a this.orders
      // @ts-ignore
      if (ids.indexOf(orders[i].id) === -1) {
        // @ts-ignore
        ids.push(orders[i].id);
        // @ts-ignore
        // console.log(orders[i].created_at);
        newOrders.push(orders[i]);
      }
    }
    return newOrders;
  }
}


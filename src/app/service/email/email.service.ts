import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SecurityService} from '../security/security.service';

@Injectable({
  providedIn: 'root'
})
export class EmailService {
  // token a envoyer dans les headers
  private token = this.security.getToken();
  // url de l'api
  private url = 'http://127.0.0.1:8000/api';
  // headers a envoyer content +token
  private headers = {headers: new HttpHeaders({
      'Content-Type': 'application/json',
       Authorization: `Bearer ${this.token}`
    })};
  constructor(private http: HttpClient, private security: SecurityService) { }
// appel vers l'api pour récuperer la liste de tous les mails envoyés
  getAllEmail() {
    return this.http.get(`${this.url}/email_sends`, this.headers).toPromise();
  }
// permet de filtrer les mails selon le status et / ou type
  getEmailFilter(emails: any, filterStat: any[], filterType: any[]) {
    const byType = this.getMailsByStatus(emails, filterStat);
    const newList = this.getMailsByType(byType, filterType);
    return newList;
  }
  // tri les mails par status et retourne la liste triée
  getMailsByStatus(emails, filterStat) {
    let mailsByStatus = [];
    // si il y au moins un filtre status checked
    if (filterStat.length > 0) {
      for (const mail of emails) {
        // pour chaque mail de la liste si le status du mail correspond a un filtre push dans mailsByStatus
        console.log(mail.status);
        console.log(filterStat.indexOf(mail.status));
        if (filterStat.indexOf(mail.status) !== -1) {
          mailsByStatus.push(mail);
        }
      }
    } else {
      mailsByStatus = emails;
    }
    return mailsByStatus;
  }
  // tri les mails par type et retourne la liste triée
  getMailsByType(emails, filterType) {
    let mailsByType = [];
    // si il y au moins un filtre type checked
    if (filterType.length > 0) {
      for (const mail of emails) {
        // pour chaque mail de la liste si le type du mail correspond a un filtre push dans mailsByType
        if (mail.type.indexOf(filterType) !== -1) {
          mailsByType.push(mail);
        }
      }
    } else {
      mailsByType = emails;
    }
    return mailsByType;
  }
// recherche par email de l'utilisateur et retourne la nouvelle liste
  getByEmail(inputEmail: string, emails) {
    const newList = [];
    for (const mail of emails) {
     if (inputEmail === mail.email) {
       newList.push(mail);
     }
   }
    return newList;
  }
}

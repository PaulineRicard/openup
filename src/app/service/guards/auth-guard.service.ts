import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private router: Router) { }

  canActivate() {
    const logged = localStorage.getItem('logged');
    if (logged === 'true') {
      console.log('canActivate loggedIN')
      return true;
    } else {
      // TODO component + route 403 !!!
      this.router.navigate(['403']).then(r => console.log(r));
    }
  }
}

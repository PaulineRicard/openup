import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Account } from '../../model/Account';


@Injectable({
  providedIn: 'root'
})


export class AccountService {
  httpOptions = {

    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })

  };


  apiUrl = 'http://127.0.0.1:8000';

  apiURL = 'http://127.0.0.1:8000/api/account';




  constructor( private http: HttpClient ) { }

 // récupère tout les utilisations non admin
  getAll(): Observable<Account[]> {
    console.log(this.apiUrl + '/api/accounts');
    return this.http.get<Account[]>(this.apiUrl + '/api/accounts');
  }

  // //récupère un utilisateur par id pour ensuite l'afficher
  // retrieve(account: Account): Observable<Account[]> {

// return this.http.get<Account[]>(`${this.apiUrl}${this.apiAccount}` + id );

  // }


  // creation d'un sponsor via un input
addsponsor(account: Account): Observable<Account> {

    return this.http.post<Account>(this.apiURL, account, this.httpOptions);

  }


//   // permet d'update un sponsor
// updateSponsor(account): Observable < Account > {

//         const url = `${this.apiUrl}/${account.id}`;
//         return this.http.put<Account>(url, this.httpOptions);

//   }


//   // permet de bloquer un utilisateur
// delete(account: Account ): Observable < Account > {

//       const url = `${this.apiUrl}/${account.id}`;
//       return this.http.delete<Account>(url, this.httpOptions);

//   }


  // activate(account:Account):Observable<Account>{
      // const url = `${this.apiUrl}/${account.id}`;
      // return this.http.activate<Account>(url, httpOptions);
  // }


  // deactivate(account:Account):Observable<Account>{
      // const url = `${this.apiUrl}/${account.id}`;
      // return this.http.deactivate<Account>(url, httpOptions);
  // }

  // checkNew(){

  // }


  // getAllSponsor(){

  // }

  // getAllUser(){

  // }

  // getAllTrader(){

  // }


  // récupère accounts par nom, prénom, email, status et role tapant dans la barre de recherche
// getByNameEmail(inputName: string, accounts); {
//
//     const newList = [];
//
//     for (const account of accounts) {
//
//      if (inputName === account.lastname) {
//        newList.push(account);
//      }
//
//      if (inputName === account.firstname ) {
//       newList.push(account);
//      }
//
//      if (inputName === account.email ) {
//       newList.push(account);
//      }
//
//      if (inputName === account.status) {
//       newList.push(account);
//      }
//
//      if (inputName === account.role) {
//       newList.push(account);
//      }
//
//    }
//
//     return newList;
//
//   }


  // permet de récupérer le type de filtre
// getTypeFilter(accounts: any, filterType: any[]); {
//
//     // const byType = this.getAccountsByStatus(accounts);
//     const newList = this.getAccountsByType(accounts , filterType);
//
//     return newList;
//
//   }


  // récupére un compte par son type: user, trader, sponsor
getAccountsByType(accounts, filterType) {

    let accountsByType = [];

    console.log(filterType);

    // si il y au moins un filtre type checked
    if (filterType.length > 0) {

      for (const account of accounts) {
        console.log(account);

        // pour chaque account de la liste si le type du account correspond a un filtre push dans accountsByType
        if (account.role.indexOf(filterType) !== -1) {
          accountsByType.push(account);
        }

        if (account.status.indexOf(filterType) !== -1) {
          accountsByType.push(account);
        }
      }

    } else {
      accountsByType = accounts;
    }

    return accountsByType;

  }

getAllChart() {
    return this.http.get(this.apiUrl + '/api/accounts').toPromise();
  }
  getById(account: string) {
    return this.http.get(`${this.apiUrl}/api/accounts/${account}`).toPromise();
  }
}

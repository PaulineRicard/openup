import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  private url = 'http://127.0.0.1:8000/api'
  constructor(private http: HttpClient) { }

  // appel vers l'api: get all type of shop
  getAll() {
    return this.http.get(`${this.url}/shop_types`).toPromise();
  }
  // appel vers l'api create shop type
  create(nameType) {
    return this.http.post(`${this.url}/shop_types`, nameType).toPromise();
  }
  // appel vers l'api: update shop type
  update(name, id) {
    return this.http.put(`${this.url}/shop_types/${id}`, name).toPromise();
  }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SecurityService} from '../security/security.service';
import {Router} from '@angular/router';
import {NgFlashMessageService} from 'ng-flash-messages';

@Injectable({
  providedIn: 'root'
})
export class MessagerieService {
  private token = this.security.getToken();
  // headers.append('Authorization', `Bearer ${tokenParse}`);
  private url = 'http://127.0.0.1:8000';
  private headers = {headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`
    })};
  // stock les messages archivés
  private archivedMsg = [];
  private unArchivedMsg = [];
  private archivedMsgConversation = [];
  private unArchivedMsgConversation = [];
  constructor(private http: HttpClient, private security: SecurityService, private router: Router, private flash: NgFlashMessageService) { }

  // appel vers l'api -> tous les messages
  // todo voir pour récuperer tous les messages non archivés + table intermédiaire + info user ??
  getAll() {
    return this.http.get(`${this.url}/api/conversations`, this.headers).toPromise();
  }
  // tri les messages reçus -> retourne uniquement les messages non lus
  // todo voir route api (pour pouvoir l'appeler de la sideBar !!!)
  getUnRead(messages) {
    const unReadMsg = [];
    console.log(messages);
    for (const msg of messages) {
      if (msg.statusAdmin === 'unread') {
        unReadMsg.push(msg);
      }
    }
    return unReadMsg;
  }
  // appel vers l'api récuperation des messages non lus => affichage notif SideBar !!!
  getNews() {
    return this.http.get(`${this.url}/api/messages/new/admin`).toPromise();
  }
// TODO voir filter après réception des données de l'api !!!
  getByFilter(filter: string, inputSearch: string, messages) {
    const newMsg = [];
    if (filter === 'lastname') {
      for (const msg of messages) {
        if (msg.lastname === inputSearch) {
          newMsg.push(msg);
        }
      }
    } else if (filter === 'id') {
      for (const msg of messages) {
        // tslint:disable-next-line:triple-equals
        if (msg.user_id == inputSearch) {
          newMsg.push(msg);
        }
      }
    }
    return newMsg;
  }
  // appel vers l'api pour archiver les messages ou récuperer en fonction du status du message
  archived(selectedMessage, status) {
    // if (selectedMessage.length > 0) {
    //   // todo voir si appel multiple de l'api ou si boucle dans l'api ??
    // }
    console.log(status);
  }
// tri les messages: stock les messages archivés dans archivedMsg et les autres dans unArchivedMsg;
  sortMessages(messages) {
    this.unArchivedMsgConversation = [];
    this.archivedMsgConversation = [];
    // ajoute une propriété select aux messages
    for (const msg of messages) {
      // console.log(typeof msg);
      if (msg.statusAdmin === 'archived') {
        this.archivedMsg.push(msg);
      } else {
        this.unArchivedMsg.push(msg);
      }
    }
  }
  getUnArchived() {
    return this.unArchivedMsg;
  }
  getArchived() {
    return this.archivedMsg;
  }
  getConversation(id) {
    return this.http.get(`${this.url}/api/conversations/${id}`).toPromise();
  }
  refresh() {
    this.unArchivedMsg = [];
    this.archivedMsg = [];
  }
  create(msg, id) {
    const message = {
      content: msg
    }
    return this.http.post(`${this.url}/api/message/${id}/admin`, message).toPromise();
  }
  sortConversation(messages) {
    this.resetArrayConversation();
    // console.log(messages);
    for (const msg of messages) {
      // console.log(typeof msg);
      if (msg.statusAdmin === 'archived') {
        this.archivedMsgConversation.push(msg);
      } else {
        this.unArchivedMsgConversation.push(msg);
      }
    }
  }

  getUnArchivedConversation() {
    return this.unArchivedMsgConversation;
  }
  resetArrayConversation() {
    this.unArchivedMsgConversation = [];
    this.archivedMsgConversation = [];
  }
// récupère le compte d'un utilisateur pour la conversation
  getUser(userId) {
    return this.http.get(`${this.url}/api/users/${userId}`).toPromise();
  }
  // récupère tous les messages d'une conversation
  getMessagesByConversation(id) {
    return this.http.get(`${this.url}/api/messages/${id}`).toPromise();
  }
}

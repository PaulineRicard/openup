import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './component/login/login.component';
import {AuthGuardService} from './service/guards/auth-guard.service';
import {HomeComponent} from './component/home/home.component';
import {AccountComponent} from './component/account/account.component';
import {OrderComponent} from './component/order/order.component';
import {GratsComponent} from './component/grats/grats.component';
import {MessagerieComponent} from './component/messagerie/messagerie.component';
import {ShopTypeComponent} from './component/shop-type/shop-type.component';
import {OrderDetailComponent} from './component/order-detail/order-detail.component';
import {EmailListComponent} from './component/email-list/email-list.component';
import {AdminComponent} from './component/admin/admin.component';
import { AccountDetailSponsorComponent } from './component/account-detail-sponsor/account-detail-sponsor.component';
import { AccountDetailUserComponent } from './component/account-detail-user/account-detail-user.component';
import { AccountDetailTraderComponent } from './component/account-detail-trader/account-detail-trader.component';
import {Page403Component} from './component/errorPage/page403/page403.component';
import {Page404Component} from './component/errorPage/page404/page404.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'login',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'admin/home',
    component: HomeComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/account',
    component: AccountComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/account/sponsor/:id',
    component: AccountDetailSponsorComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/account/user/:id',
    component: AccountDetailUserComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/account/trader/:id',
    component: AccountDetailTraderComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/order',
    component: OrderComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/grats',
    component: GratsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/messagerie',
    component: MessagerieComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/email',
    component: EmailListComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/shoptype',
    component: ShopTypeComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'admin/order/:id',
    component: OrderDetailComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin/admin',
    component: AdminComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: '403',
    component: Page403Component,
  },
  {
    path: '404',
    component: Page404Component,
  },
  { path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
